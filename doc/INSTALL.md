# Installation of the SSIMS Toolbox #

The toolbox is completely implemented in MATLAB. To get started, just add the
sub-folder `for_MATLAB` to your MATLAB path. The `examples` folder contains
a sample dataset and a script that can be run out-of-the-box.
However, the basic MATLAB functions are rather slow.
Therefore, core functionality has also been implemented in C/C++ and can
be compiled as `mex` files. This will improve performance dramatically
(~2 orders of magnitude)
See compilation instructions for your platform below.

## Documentation ##

The C/C++ implementation is rudimentarily documented. HTML and LaTeX representations can be created with [`doxygen`](http://www.stack.nl/~dimitri/doxygen/), e.g. with

    make docs

MATLAB functions (and their `.mex` equivalents) are documented in the `.m` files.
See the `examples` folder for a self-contained example with actual data.

## How to compile MEX files included in the SSIMS_TOOLBOX ##

Several heavily used and computationally expensive functions have been rewritten
in C/C++ and are available as MEX files. MEX files need to be recompiled for the
computer they are to be used on. MATLAB provides instructions and documentation
on how to setup the compilation environment for different machines and operating
systems.

For best results, use [openmp](http://openmp.org) for parallelized code
execution. An example configuration file for MATLAB 2015b on Mac OS X 10.11
using `clang-omp` installed with [homebrew](http://brew.sh) has been included
with this toolbox (`mex_C++_maci64.xml`).

This toolbox relies on the [Armadillo linear algebra
library](http://arma.sourceforge.net), which has to be installed first.

### On Linux / Mac: ###

#### Mac OS X
This procedure has been tested to work with Mac OS X 10.9, 10.10, and 10.11,
with Xcode 6.2, 7.3, and 7.3, respectively. MATLAB 2016a is confirmed to work
on all these systems, MATLAB 2015b has only been tested on 10.11.

1. Install [Xcode](https://developer.apple.com/xcode/), from the
    [App Store](https://itunes.apple.com/us/app/xcode/id497799835?mt=12) or by download.
    Run it at least once to accept license agreement. Depending on your version of MATLAB,
    only some versions of Xcode are compatible. E.g. MATLAB 2014b only works with Xcode 4.6+ or 5.0+,
    but not 6.0+ or later. MATLAB 2015b supports Xcode 5.1+ or later, but [requires additional setup for 7.0+](http://www.mathworks.com/support/sysreq/files/SystemRequirements-Release2015b_SupportedCompilers.pdf).
    Make sure that Xcode knows about its command line tools: open Xcode, open Xcode->Preferences, go to the
    'Locations' pane, ensure that 'Xcode 7.3.1' (or whatever your version of Xcode is installed) is selected
    under 'Command Line Tools'.
    MATLAB 2016a requires Xcode 6.0+ or 7.0+ and is used here.
    If later on during compilation you receive an error message stating '`cstring.h` not found' or similar, try to run `xcode-select --install` from Terminal.
2. Install [`homebrew`](http://brew.sh), the missing package manager for Mac OS.
    (Open `Terminal` and follow the instructions at the above website.)
3. Install [Armadillo](http://arma.sourceforge.net), a fast linear algebra package for C++.
    Still in `Terminal`, enter

        brew install homebrew/science/armadillo

    This step may take a long time, because `gcc`, the GNU com
4. In `Terminal`, navigate to your home folder (`cd ~`). Enter `ls -al` to check if the file [`.matlab7rc.sh`](http://www.mathworks.com/help/matlab/ref/matlabmac.html#bunueg9-5) exists.
    (Note that it is a hidden file, so normally will not appear in Finder. The name of the file is important:
    MATLAB *only* looks for `~/.matlab7rc.sh`!)
    * If it does exist, open it in a text editor and change the line defining `LDPATH_PREFIX` in the
      `mac|maci|maci64)` section (around line 195) to read

            LDPATH_PREFIX='/usr/local/lib/gcc/6'

      (The path to `gcc` may change, as of writing, this is where it will be installed by `homebrew`)
    * If the file does not exist, copy the file `SSIMS_toolbox/doc/matlab7rc.sh` to `~/.matlab7rc.sh`:

            cp SSIMS_toolbox/doc/install_mac/matlab7rc.sh ~/.matlab7rc.sh

      where `SSIMS_toolbox` corresponds to the path of this toolbox

5. Open or restart MATLAB
6. Change working directory to the SSIMS toolbox path
7. On the MATLAB prompt, enter

        copyfile localdefs_sample.mk localdefs.mk
        edit localdefs.mk

8. Make changes for your system. In particular, adjust `MATLABPATH` to point to your
    installation of MATLAB.
9. In `Terminal`, navigate to the `SSIMS_toolbox` folder and enter

        make

    This should compile all necessary files. Compiled `*.mex*` files will be automatically placed in `for_MATLAB`.
    Enjoy the speed!

To use parallel processing, your compiler needs to support parallelized code. The `clang/LLVM` compiler bundled with Xcode <= 7.3.1 does not support OpenMP. However, it is easy to install a different version of `LLVM` and tell MATLAB to use it.

1. Install the current version of `llvm` from `brew` (from `Terminal` call):

        brew install llvm

2. Copy the file `doc/install_mac/clang++_openmp_maci64.xml` in this package to your MATLAB `prefdir()`:

        cp SSIMS_toolbox/doc/install_mac/clang++_openmp_maci64.xml ~/.matlab/R2016/

    where `SSIMS_toolbox` corresponds to the path of this toolbox

3. Tell MATLAB to use this configuration: Call

        mex -setup:~/.matlab/R2016a/clang++_openmp_maci64.xml C++

    from the MATLAB prompt
4. If you copied the `.matlab7rc.sh` startup script in step 4 above, continue with step 6. If you made changes to a
    previous file, you will have to add a folder to `LDPATH_PREFIX`; this line should now read:

        LDPATH_PREFIX='/usr/local/lib/gcc/6:/usr/local/opt/llvm/lib'
5. Restart MATLAB
6. Edit your `localdefs.mk` file to uncomment the options after the `## Using parallelization techniques.` line
7. In `Terminal`, navigate to the `SSIMS_toolbox` folder and enter

        make clean
        make
8. In MATLAB, run `SSIMSToolboxTestParallel`. If it was compiled correctly it will display the number of parallel threads.

Note that an upgrade to macOS 12 may break things: linking against `libomp` included with `llvm` will crash MATLAB. Instead, use the build configuration in `clang++_openmp_macOS12_maci64.xml` and make changes to `localdefs.mk`, as shown in the sample file. Furthermore, `llvm` may have to be reinstalled (`brew reinstall llvm`).

With questions regarding the compilation step, <dr.jonas@brown.edu> may be able to help.

#### Linux
For now, you're on your own. Ensure the dependencies (`armadillo`, `clang` or another
parallelizable, OpenMP-compatible compiler) are satisfied. Copy `localdefs_sample.mk` to
`localdefs.mk` and make appropriate changes. Run

    make all

### On Windows: ###

1. Download the [Armadillo library](http://arma.sourceforge.net/download.html)
  and unpack the archive. Rename the unpacked folder to `armadillo` and move
  it into the `lib` folder of this toolbox.
2. Make sure armadillo is correctly configured to use MATLAB's LAPACK
  and BLAS libraries. Armadillo's configuration file is at
  `armadillo\include\armadillo_bits\config.hpp`.

  With our installation (MATLAB 2015b 64bit), line 69 for version 7.400.2
  had to be uncommented to read

        #define ARMA_BLAS_LONG_LONG

  Also, line 62 had to be commented out:

        //#define ARMA_BLAS_UNDERSCORE

3. For compilation, we have used MinGW64 as it is available through MATLAB's
  AddOn system. Make sure it is installed correctly and mex uses it (to change,
  execute `mex -setup C++` and `mex -setup C` from the MATLAB prompt).
  Within MATLAB, navigate to the SSIMS toolbox folder. Adjust the

        build_files_win.m

  script to match your system's circumstances (If you use `armadillo`
  and MinGW64 as described, you should not have to change anything).
  Run `build_files_win.m` to compile. Build products will be automatically
  copied into the `for_MATLAB` folder.

Currently, parallelization using the [OpenMP API](http://openmp.org/wp/) has
not been tested under Windows. If you have compiled this toolbox successfully
with parallelization, please let me (Jonas Zimmermann, <dr.jonas@brown.edu>) know.
