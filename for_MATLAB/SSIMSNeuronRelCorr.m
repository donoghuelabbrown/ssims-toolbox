function [ ncmat, dmatnb, basespiketrains] = SSIMSNeuronRelCorr(sorted_timestamps, ev, stduration, q)
%%  SPIKE TRAIN SIMILARITY SPACE TOOLBOX% 
% [ncmat] = SSIMSNeuronRelCorr (sorted_timestamps, event, stduration, q)
% helper function for SSIMSETS
%
% @author Carlos Vargas-Irwin
% Copyright (c) Carlos Vargas-Irwin, Brown University. All rightsreserved. Resistance is futile.


%% calculate pairwise distance matrix for each neuron  
[dmatnb, basespiketrains] = getSSIMDMatBetweenTimePoints(sorted_timestamps, ev, stduration, q);

%% calculate correlation between relational mapping vectors (neurons)
nn = length(sorted_timestamps); % number of neurons
bl = size(dmatnb,1);

ncmat = zeros(nn,nn);
ix1 = [1:bl];
for n1 = 1:nn

    ix2 = [1:bl];
    for n2 = 1:nn
        
        if (n1<=n2)
         ncmat(n1,n2) =  corr2( reshape(dmatnb(:,ix1),1,[]),reshape(dmatnb(:,ix2),1,[])  );
        else
          ncmat(n1,n2) =   ncmat(n2,n1);
        end
        ix2 = ix2+bl; 
        
    end
        ix1 = ix1+bl;
end 


