function [ SNETS ] = SSIMNETS(sorted_timestamps, event, stduration, q, neurondim, perp, crange,varargin)
%  SPIKE TRAIN SIMILARITY SPACE TOOLBOX% 
%  OVERVIEW:
%  This function identifies 'functional sub-ensembles' (sub-nets)
%  This is done by clustering neurons according to the correlation between
%  single unit SSIMS relational maps
%  NOTE: the firing patterns are not compared directly! Only in terms of
%  their SSIMS representation. So: neurons that rank the same trials as
%  being similar will be clustered together (even if their firing patterns
%  are different.
%  Neurons are clustered using K-means, the number of clusters is selected 
%  using silhouette values 
%  (high values ~ higer between/within cluster distances)
%
% INPUTS: (all times in seconds)
% sorted_timestamps = cell array with the sorted timestamps for each neuron (length = n)
% event = timestamps for events to align spike trains to (length = m)
% stduration = duration of spike trains to be analyzed
% q = from Victor & Purpura's algorithm, 1/q specifies temporal precision
% neurondim  = # of dimensions used to cluster neurons
% perp = perplexity used to cluster neurons
% crange = # of clusters to check using k means
% displayopt = the last (optional) argument will plot results if set to 1
%
% Code requires SSIMS toolbox + helper functions:
%   - SSIMSNeuronRelCorr
%   - autokmeanscluster
%
% OUTPUTS: 
% SNETS.NSPACE: each point represents a neuron
% SNETS.clusterindex: index denoting membership in a cluster
% SNETS.centroids: cluster centroids
% SNETS.silhouette: silhouette values for different # of clusters identified using k-means (Higher = more cluster separation)
% SNETS.numclus: # of clusters identified (NOTE: this version will only detect > 1 cluster!!!)
% SNETS.NNcorr: Correlations between single unit SSIMS relational maps ( neuron x neuron )
% SNETS.distmat: full distance matrix ( neuron x events )
%
% @author Carlos Vargas-Irwin
% Copyright (c) Carlos Vargas-Irwin, Brown University. All rightsreserved. Resistance is futile.

% Create a SSIMS relational map for each neuron
% calculate correlations between them
[ ncmat, dmatnb ] = SSIMSNeuronRelCorr(sorted_timestamps, event, stduration, q);

% Use t-SNE to project the matrix of neuron-neuron correlations into
% a low dimensional space
[NSPACE, tSNE_transform, kld] = runtSNE(ncmat, neurondim, perp);


% Cluster using K-means, selecting the number of clusters
[s cindex centroids nclus ] = autokmeanscluster(crange,NSPACE);

% put results in a nice data structure
SNETS.NSPACE = NSPACE;
SNETS.clusterindex = cindex;
SNETS.centroids = centroids;
SNETS.silhouette = s;
SNETS.numclus = nclus;
SNETS.NNcorr = ncmat;
SNETS.distmat = dmatnb;
SNETS.NStransform = tSNE_transform;


% to plot... or not
if nargin>7
    plotfig = varargin{1};
else
    plotfig = 0;
end


if plotfig
    
figure
plot(2:max(crange),s(2:max(crange)),'ko-')
set(gca,'fontsize',20)
xlabel('cluster #')
ylabel('mean sil. values')
    
figure
plotNT(NSPACE  ,cindex','symbol','o','msize',15);
view(2)

hold on
for n = 1:nclus
    h = text(centroids(n,1),centroids(n,2),num2str(n)); set(h,'color','m','fontsize',30)
    hold on  
end

end






